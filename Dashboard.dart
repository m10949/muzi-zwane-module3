import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold (
          appBar: AppBar(
            title: const Text ("Dashboard"),
          ), //appBar
          body: Center(child: Text("welcome to the Dashboard")),
             floatingActionButton: FloatingActionButton (
                onPressed: () => {},
                child: const Text("Login page"),
             ), //floating button

             floatingActionButton: FloatingActionButton (
               onPressed: () => {print('go to the profile page')},
                child: const Text("Profile"),
             ) //floating button
          ),
        )
      );
  }
}
