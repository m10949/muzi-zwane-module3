import 'package:flutter/material.dart';

class profileEdit extends StatelessWidget {
  const profileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold (
          appBar: AppBar(
            title: const Text ("User Profile"),

          ),
          
          body: Center(child: Text ("welcome to the user profile page")),
          floatingActionButton: FloatingActionButton(
            onPressed: () => {print('go back to dashboard')},
            child: const Text ("dashboard")
          )
        )
    );
  }
}
